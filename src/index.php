<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title>Cadastro de Usuário</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
</head>

<body>
    <?php        
        //ini_set('display_errors', '1'); 
        if(isset($_SESSION['msg'])){
            echo $_SESSION['msg'];
            unset($_SESSION['msg']);
        }       
    ?>

    <div class="container">
        <form action="register.php" method="post">
            <center><h2>Cadastrar Usuário</h2></center>
            <div class="form-group">
                <div >
                <label for="inputName1">Nome:</label>
                <input type="text" name="name" id="inputName" class="form-control"/>
            </div>
            <div class="form-group">                
                <label for="inputEmail1">E-mail:</label>
                <input type="email" name="email" id="inputEmail" class="form-control"/>
            </div>        
            <div class="form-group">                
                <label for="inputCpf">Cpf:</label>
                <input type="text" name="txt_cpf" id="inputCpf" class="form-control"/>
            </div>
            <div class="form-group">                
                <label for="inputCnpj">Cnpj</label>
                <input type="text" name="txt_cnpj" id="inputCnpj" class="form-control"/>
            </div>
            <div class="form-group">
                <div class=".col-md-3 .offset-md-3" >
                <label for="selectTipo">Tipo de Usuário
                    <select class="form-control" name="tipo" id="selectTipo">
                        <option value="01">Usuário Comum</option>
                        <option value="02">Usuário Administrador</option>              
                    </select>
                </label>
                </div>
            <div class="form-group">
                <div class="col-md-6 offset-md-3"style="text-align:center; margin: 0 auto;">
                <input class="btn btn-primary" name="btn_submit" type="submit" value="Cadastrar"/>
            </div>
        </form>
    </div>
  
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    

</body>

</html>
